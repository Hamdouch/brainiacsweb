<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FOS\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class RegistrationFormType extends AbstractType
{
    /**
     * @var string
     */
    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom',null, array('label' => 'form.nom', 'translation_domain' => 'FOSUserBundle','attr' => array(
                'type' => 'text',
                'class' => 'form-group',
                'placeholder' => 'Votre Nom'
            ),))
            ->add('prenom',null, array('label' => 'form.prenom', 'translation_domain' => 'FOSUserBundle','attr' => array(
                'type' => 'text',
                'class' => 'form-group',
                'placeholder' => 'Votre Prenom'
            ),))
            ->add('datedenaissance',null, array('label' => 'form.datedenaissance', 'translation_domain' => 'FOSUserBundle','attr' => array(
                'type' => 'date',
                'class' => 'form-group',
                'placeholder' => 'Votre Date De Naissance'
            ),))
            ->add('email', EmailType::class, array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle','attr' => array(
                'class' => 'form-group',
                'placeholder' => 'Votre mail'
            ),))
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle','attr' => array(
                'type' => 'text',
                'class' => 'form-group',
                'placeholder' => 'Username'
            ),))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'autocomplete' => 'new-password',
                        'class' => 'form-group',
                        'placeholder' => 'mot de passe'
                    ),
                ),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('roles', ChoiceType::class, array('label' => 'Type ', 'choices' => array('ETABLISSEMENT' => 'ROLE_ETAB', 'CLIENT' => 'ROLE_USER'), 'required' => true, 'multiple' => true))


            ->add('photo',FileType::class, array('attr' => array('class' => 'form-control'),'data_class' => null),'Symfony\Component\Form\Extension\Core\Type\FileType', array('label' => 'Image'));
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_token_id' => 'registration',
        ));
    }

    // BC for SF < 3.0

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fos_user_registration';
    }
}
