<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'SBC\\NotificationsBundle' => array($vendorDir . '/mrad/notifications-bundle'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src'),
    'PhpCollection' => array($vendorDir . '/phpcollection/phpcollection/src'),
    'Nomaya\\SocialBundle\\' => array($vendorDir . '/nomaya/social-bundle'),
    'Metadata\\' => array($vendorDir . '/jms/metadata/src'),
    'Knp\\Component' => array($vendorDir . '/knplabs/knp-components/src'),
    'JsonpCallbackValidator' => array($vendorDir . '/willdurand/jsonp-callback-validator/src'),
    'JMS\\Serializer' => array($vendorDir . '/jms/serializer/src'),
    'JMS\\' => array($vendorDir . '/jms/parser-lib/src'),
    'FOS\\MessageBundle' => array($vendorDir . '/friendsofsymfony/message-bundle'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
);
