<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // user_homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'UserBundle\\Controller\\DefaultController::indexAction',  '_route' => 'user_homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_user_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'user_homepage'));
            }

            return $ret;
        }
        not_user_homepage:

        if (0 === strpos($pathinfo, '/front')) {
            // front_homepage
            if ('/front' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'FrontBundle\\Controller\\DefaultController::indexAction',  '_route' => 'front_homepage',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_front_homepage;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'front_homepage'));
                }

                return $ret;
            }
            not_front_homepage:

            // navArtIA
            if ('/front/artia' === $pathinfo) {
                return array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::artIAAction',  '_route' => 'navArtIA',);
            }

            // articleDetails
            if (preg_match('#^/front/(?P<id>[^/]++)/artDetails$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'articleDetails']), array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::articleDetailsAction',));
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_articleDetails;
                }

                return $ret;
            }
            not_articleDetails:

            // deleteComment
            if (preg_match('#^/front/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'deleteComment']), array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::deleteAction',));
            }

            // deleteCommentev
            if (preg_match('#^/front/(?P<id>[^/]++)/deletev$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'deleteCommentev']), array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::deleteAction',));
            }

            if (0 === strpos($pathinfo, '/front/a')) {
                // navArtRob
                if ('/front/artrob' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::artRobAction',  '_route' => 'navArtRob',);
                }

                // navAjoutEvent
                if ('/front/ajoutEvent' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::addEventAction',  '_route' => 'navAjoutEvent',);
                }

                // navAffichageEvent
                if ('/front/affichageEvent' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::listeEventAction',  '_route' => 'navAffichageEvent',);
                }

            }

            elseif (0 === strpos($pathinfo, '/front/event')) {
                // navEventEvenement
                if ('/front/eventEvenement' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle:Navigation:eventEvenement',  '_route' => 'navEventEvenement',);
                }

                // navEventConf
                if ('/front/eventconf' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle:Navigation:eventConf',  '_route' => 'navEventConf',);
                }

                // navEventComp
                if ('/front/eventcomp' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle:Navigation:eventComp',  '_route' => 'navEventComp',);
                }

            }

            elseif (0 === strpos($pathinfo, '/front/for')) {
                // navForBI
                if ('/front/forbi' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\FormationFrontController::forBIAction',  '_route' => 'navForBI',);
                }

                // navForDS
                if ('/front/fords' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\FormationFrontController::forDSAction',  '_route' => 'navForDS',);
                }

                // navForIA
                if ('/front/foria' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\FormationFrontController::forIAAction',  '_route' => 'navForIA',);
                }

                // navForRec
                if ('/front/forrec' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\ClaimevFrontController::forRecAction',  '_route' => 'navForRec',);
                }

            }

            // claim_delete
            if (0 === strpos($pathinfo, '/front/claim') && preg_match('#^/front/claim/(?P<idclaim>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'claim_delete']), array (  '_controller' => 'FrontBundle:Claim:delete',));
                if (!in_array($requestMethod, ['DELETE'])) {
                    $allow = array_merge($allow, ['DELETE']);
                    goto not_claim_delete;
                }

                return $ret;
            }
            not_claim_delete:

            // profile
            if ('/front/Profil' === $pathinfo) {
                return array (  '_controller' => 'FrontBundle\\Controller\\ProfileController::showProfileAction',  '_route' => 'profile',);
            }

            // navSuppressionEvent
            if ('/front/suppressionEvent' === $pathinfo) {
                return array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::deleteEventAction',  '_route' => 'navSuppressionEvent',);
            }

            // navModificationEvent
            if ('/front/modifEvent' === $pathinfo) {
                return array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::updateEventAction',  '_route' => 'navModificationEvent',);
            }

            // navShowOneEvent
            if (preg_match('#^/front/(?P<id>[^/]++)/oneEvent$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'navShowOneEvent']), array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::oneEventAction',));
            }

            if (0 === strpos($pathinfo, '/front/pa')) {
                // navPartEvent
                if ('/front/partEvent' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::participantEventAction',  '_route' => 'navPartEvent',);
                }

                // participerFor
                if ('/front/participer' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\FormationFrontController::participerForAction',  '_route' => 'participerFor',);
                }

                // navPayEvent
                if ('/front/payEvent' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::payEventAction',  '_route' => 'navPayEvent',);
                }

                if (0 === strpos($pathinfo, '/front/payement')) {
                    // testpayement
                    if (preg_match('#^/front/payement/(?P<numCarte>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'testpayement']), array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::testerpayementAction',));
                    }

                    // payer
                    if ('/front/payementval' === $pathinfo) {
                        return array (  '_controller' => 'FrontBundle\\Controller\\EvenementController::payAction',  '_route' => 'payer',);
                    }

                }

            }

            // navForRob
            if ('/front/forrob' === $pathinfo) {
                return array (  '_controller' => 'FrontBundle\\Controller\\FormationFrontController::forRobAction',  '_route' => 'navForRob',);
            }

            if (0 === strpos($pathinfo, '/front/a')) {
                // navAjoutFor
                if ('/front/ajoutFor' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\FormationFrontController::addForAction',  '_route' => 'navAjoutFor',);
                }

                // actualiteajoutetab
                if ('/front/ajouterActualitefr' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::ajoutactualitefrontAction',  '_route' => 'actualiteajoutetab',);
                }

                // actualitefrontaffichage
                if ('/front/actualiteaffich' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::affichagedesactualiteAction',  '_route' => 'actualitefrontaffichage',);
                }

                // affichageactualiteparevent
                if ('/front/affichageevent' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::affichagepareventAction',  '_route' => 'affichageactualiteparevent',);
                }

                if (0 === strpos($pathinfo, '/front/art')) {
                    // navAjoutArticleFront
                    if ('/front/artaddfront' === $pathinfo) {
                        return array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::addarticlefAction',  '_route' => 'navAjoutArticleFront',);
                    }

                    // navAfficherArticleFront
                    if ('/front/artlistfront' === $pathinfo) {
                        return array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::afficherFrontAction',  '_route' => 'navAfficherArticleFront',);
                    }

                    // navModifierArticleFront
                    if ('/front/artmodifront' === $pathinfo) {
                        return array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::modifierFrontAction',  '_route' => 'navModifierArticleFront',);
                    }

                    // navSupprimerArticleFront
                    if ('/front/artdeletefront' === $pathinfo) {
                        return array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::supprimerFrontAction',  '_route' => 'navSupprimerArticleFront',);
                    }

                    // navShowOneArt
                    if ('/front/artshowonefront' === $pathinfo) {
                        return array (  '_controller' => 'FrontBundle\\Controller\\ArticleFrontController::pdfAction',  '_route' => 'navShowOneArt',);
                    }

                }

            }

            // modifierFor
            if (0 === strpos($pathinfo, '/front/ModifierFor') && preg_match('#^/front/ModifierFor/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'modifierFor']), array (  '_controller' => 'FrontBundle\\Controller\\FormationFrontController::ModifierForAction',));
            }

            // consulteractualiteinfront
            if (0 === strpos($pathinfo, '/front/consulteractualite') && preg_match('#^/front/consulteractualite/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'consulteractualiteinfront']), array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::consulteractualiteinfrontAction',));
            }

            // testreviewplacement
            if ('/front/testerr' === $pathinfo) {
                return array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::testreviewAction',  '_route' => 'testreviewplacement',);
            }

            if (0 === strpos($pathinfo, '/front/list')) {
                // actualiteajouterparetab
                if ('/front/list' === $pathinfo) {
                    return array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::affichageetabcoAction',  '_route' => 'actualiteajouterparetab',);
                }

                // modifieractualtetab
                if (0 === strpos($pathinfo, '/front/listmodif') && preg_match('#^/front/listmodif/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'modifieractualtetab']), array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::modifieractetabAction',));
                }

            }

            // supprimeractualtetab
            if (0 === strpos($pathinfo, '/front/supprimerActetab') && preg_match('#^/front/supprimerActetab/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'supprimeractualtetab']), array (  '_controller' => 'FrontBundle\\Controller\\ActualitefrontController::suppactetabAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/back')) {
            // back_homepage
            if ('/back' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'BackBundle\\Controller\\DefaultController::indexAction',  '_route' => 'back_homepage',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_back_homepage;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'back_homepage'));
                }

                return $ret;
            }
            not_back_homepage:

            if (0 === strpos($pathinfo, '/back/a')) {
                if (0 === strpos($pathinfo, '/back/admin')) {
                    if (0 === strpos($pathinfo, '/back/adminevent')) {
                        // navAddBackEvent
                        if ('/back/admineventadd' === $pathinfo) {
                            return array (  '_controller' => 'BackBundle\\Controller\\BackEventController::addTechEventAction',  '_route' => 'navAddBackEvent',);
                        }

                        // navListBackEvent
                        if ('/back/admineventlist' === $pathinfo) {
                            return array (  '_controller' => 'BackBundle\\Controller\\BackEventController::listeEventBackAction',  '_route' => 'navListBackEvent',);
                        }

                        // navUpdateBackEvent
                        if ('/back/admineventup' === $pathinfo) {
                            return array (  '_controller' => 'BackBundle\\Controller\\BackEventController::updateBackEventAction',  '_route' => 'navUpdateBackEvent',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/back/admindelete')) {
                        // navDeleteBackEvent
                        if ('/back/admindelete' === $pathinfo) {
                            return array (  '_controller' => 'BackBundle\\Controller\\BackEventController::backDeleteEventAction',  '_route' => 'navDeleteBackEvent',);
                        }

                        // navDeletePart
                        if ('/back/admindeletepart' === $pathinfo) {
                            return array (  '_controller' => 'BackBundle\\Controller\\BackEventController::partDeleteAction',  '_route' => 'navDeletePart',);
                        }

                    }

                    // navListePart
                    if ('/back/adminlisteepart' === $pathinfo) {
                        return array (  '_controller' => 'BackBundle\\Controller\\BackEventController::listePartBackAction',  '_route' => 'navListePart',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/back/add')) {
                    // navAddArticle
                    if ('/back/addArticle' === $pathinfo) {
                        return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::addArticleAction',  '_route' => 'navAddArticle',);
                    }

                    // ajouteractualite
                    if ('/back/addact' === $pathinfo) {
                        return array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::ajouteractAction',  '_route' => 'ajouteractualite',);
                    }

                    // ajoutercategorie
                    if ('/back/addcat' === $pathinfo) {
                        return array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::ajoutercategAction',  '_route' => 'ajoutercategorie',);
                    }

                }

                // espaceAjout
                if ('/back/ajoutArticle' === $pathinfo) {
                    return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::addArticleAction',  '_route' => 'espaceAjout',);
                }

                // archiverArticle
                if ('/back/archiverArt' === $pathinfo) {
                    return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::archiverArticleAction',  '_route' => 'archiverArticle',);
                }

                // afficherallactualite
                if ('/back/allactualite' === $pathinfo) {
                    return array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::affichallactAction',  '_route' => 'afficherallactualite',);
                }

                // afficheradminactualite
                if ('/back/affiche' === $pathinfo) {
                    return array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::affadminactualiteAction',  '_route' => 'afficheradminactualite',);
                }

            }

            elseif (0 === strpos($pathinfo, '/back/s')) {
                if (0 === strpos($pathinfo, '/back/search')) {
                    // ajax_search
                    if ('/back/search' === $pathinfo) {
                        return array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::searchAction',  '_route' => 'ajax_search',);
                    }

                    // ajax_searchingwael
                    if ('/back/searchg' === $pathinfo) {
                        return array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::searchAction',  '_route' => 'ajax_searchingwael',);
                    }

                }

                // statgen
                if ('/back/statgener' === $pathinfo) {
                    return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::afficherStatGenAction',  '_route' => 'statgen',);
                }

                // supprimeractualt
                if (0 === strpos($pathinfo, '/back/supprimerActualite') && preg_match('#^/back/supprimerActualite/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'supprimeractualt']), array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::supprimeractAction',));
                }

                // suppcateg
                if (0 === strpos($pathinfo, '/back/supprimerCategorie') && preg_match('#^/back/supprimerCategorie/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'suppcateg']), array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::supprimercatAction',));
                }

            }

            // test
            if ('/back/go' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\DefaultController::goAction',  '_route' => 'test',);
            }

            // listeLinda
            if ('/back/liste' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\FormationBackController::listeForAction',  '_route' => 'listeLinda',);
            }

            // listactualitearchiver
            if ('/back/listdesArchive' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::listarchiveactAction',  '_route' => 'listactualitearchiver',);
            }

            // ModifierFor
            if ('/back/Modifier' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\FormationBackController::modifierForAction',  '_route' => 'ModifierFor',);
            }

            // SupprimerFor
            if ('/back/Supprimer' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\FormationBackController::supprimerForAction',  '_route' => 'SupprimerFor',);
            }

            // tableBasic
            if ('/back/tableDebase' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::afficherAction',  '_route' => 'tableBasic',);
            }

            // tablearchive
            if ('/back/tablearchive' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::afficherArchiveAction',  '_route' => 'tablearchive',);
            }

            // deleteArticle
            if ('/back/delete' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::deleteAction',  '_route' => 'deleteArticle',);
            }

            // updateArticle
            if ('/back/updateArtic' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::updateAction',  '_route' => 'updateArticle',);
            }

            // tableBasicmj
            if ('/back/incrnbrvue' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::incrementNbrVueAction',  '_route' => 'tableBasicmj',);
            }

            if (0 === strpos($pathinfo, '/back/claim')) {
                // claim_new
                if ('/back/claim/new' === $pathinfo) {
                    $ret = array (  '_controller' => 'FrontBundle\\Controller\\ClaimevFrontController::newAction',  '_route' => 'claim_new',);
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_claim_new;
                    }

                    return $ret;
                }
                not_claim_new:

                // claim_show
                if (preg_match('#^/back/claim/(?P<idclaimev>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'claim_show']), array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::showAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_claim_show;
                    }

                    return $ret;
                }
                not_claim_show:

                // claim_block
                if (preg_match('#^/back/claim/(?P<idclaimev>[^/]++)/block$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'claim_block']), array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::blockAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_claim_block;
                    }

                    return $ret;
                }
                not_claim_block:

                // claim_deblock
                if (preg_match('#^/back/claim/(?P<idclaimev>[^/]++)/deblock$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'claim_deblock']), array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::deblockAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_claim_deblock;
                    }

                    return $ret;
                }
                not_claim_deblock:

                // claim_index
                if ('/back/claim' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::indexAction',  '_route' => 'claim_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_claim_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'claim_index'));
                    }

                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_claim_index;
                    }

                    return $ret;
                }
                not_claim_index:

                // claim_sort
                if (0 === strpos($pathinfo, '/back/claim/sort') && preg_match('#^/back/claim/sort/(?P<sort>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'claim_sort']), array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::sortAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_claim_sort;
                    }

                    return $ret;
                }
                not_claim_sort:

                // claim_approve
                if (preg_match('#^/back/claim/(?P<idclaimev>[^/]++)/approve$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'claim_approve']), array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::approveAction',));
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_claim_approve;
                    }

                    return $ret;
                }
                not_claim_approve:

                // claim_ignore
                if (preg_match('#^/back/claim/(?P<idclaimev>[^/]++)/ignore$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'claim_ignore']), array (  '_controller' => 'BackBundle\\Controller\\ClaimevBackController::ignoreAction',));
                    if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                        $allow = array_merge($allow, ['GET', 'POST']);
                        goto not_claim_ignore;
                    }

                    return $ret;
                }
                not_claim_ignore:

            }

            // consultactualite
            if (0 === strpos($pathinfo, '/back/consulteractualite') && preg_match('#^/back/consulteractualite/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'consultactualite']), array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::consulteractAction',));
            }

            // pdfviw
            if ('/back/pdfv' === $pathinfo) {
                return array (  '_controller' => 'BackBundle\\Controller\\ArticleBackController::pdfAction',  '_route' => 'pdfviw',);
            }

            // modifieractualt
            if (0 === strpos($pathinfo, '/back/modifierActualite') && preg_match('#^/back/modifierActualite/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'modifieractualt']), array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::modifieractAction',));
            }

            // ArchiverActualite
            if (0 === strpos($pathinfo, '/back/ArchiverActualite') && preg_match('#^/back/ArchiverActualite/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'ArchiverActualite']), array (  '_controller' => 'BackBundle\\Controller\\ActualitebackController::archiveractAction',));
            }

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        if (0 === strpos($pathinfo, '/login')) {
            // fos_user_security_login
            if ('/login' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:loginAction',  '_route' => 'fos_user_security_login',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_security_login;
                }

                return $ret;
            }
            not_fos_user_security_login:

            // fos_user_security_check
            if ('/login_check' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:checkAction',  '_route' => 'fos_user_security_check',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_security_check;
                }

                return $ret;
            }
            not_fos_user_security_check:

        }

        // fos_user_security_logout
        if ('/logout' === $pathinfo) {
            $ret = array (  '_controller' => 'fos_user.security.controller:logoutAction',  '_route' => 'fos_user_security_logout',);
            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                $allow = array_merge($allow, ['GET', 'POST']);
                goto not_fos_user_security_logout;
            }

            return $ret;
        }
        not_fos_user_security_logout:

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if ('/profile' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'fos_user.profile.controller:showAction',  '_route' => 'fos_user_profile_show',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_fos_user_profile_show;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_user_profile_show'));
                }

                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_profile_show;
                }

                return $ret;
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ('/profile/edit' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.profile.controller:editAction',  '_route' => 'fos_user_profile_edit',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_profile_edit;
                }

                return $ret;
            }
            not_fos_user_profile_edit:

            // fos_user_change_password
            if ('/profile/change-password' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.change_password.controller:changePasswordAction',  '_route' => 'fos_user_change_password',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_change_password;
                }

                return $ret;
            }
            not_fos_user_change_password:

        }

        elseif (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if ('/register' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'fos_user.registration.controller:registerAction',  '_route' => 'fos_user_registration_register',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_fos_user_registration_register;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_user_registration_register'));
                }

                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_registration_register;
                }

                return $ret;
            }
            not_fos_user_registration_register:

            // fos_user_registration_check_email
            if ('/register/check-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.registration.controller:checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_registration_check_email;
                }

                return $ret;
            }
            not_fos_user_registration_check_email:

            if (0 === strpos($pathinfo, '/register/confirm')) {
                // fos_user_registration_confirm
                if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_user_registration_confirm']), array (  '_controller' => 'fos_user.registration.controller:confirmAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_fos_user_registration_confirm;
                    }

                    return $ret;
                }
                not_fos_user_registration_confirm:

                // fos_user_registration_confirmed
                if ('/register/confirmed' === $pathinfo) {
                    $ret = array (  '_controller' => 'fos_user.registration.controller:confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_fos_user_registration_confirmed;
                    }

                    return $ret;
                }
                not_fos_user_registration_confirmed:

            }

        }

        elseif (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ('/resetting/request' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:requestAction',  '_route' => 'fos_user_resetting_request',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_request;
                }

                return $ret;
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_user_resetting_reset']), array (  '_controller' => 'fos_user.resetting.controller:resetAction',));
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_resetting_reset;
                }

                return $ret;
            }
            not_fos_user_resetting_reset:

            // fos_user_resetting_send_email
            if ('/resetting/send-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_resetting_send_email;
                }

                return $ret;
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ('/resetting/check-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_check_email;
                }

                return $ret;
            }
            not_fos_user_resetting_check_email:

        }

        elseif (0 === strpos($pathinfo, '/message')) {
            // fos_message_inbox
            if ('/message' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::inboxAction',  '_route' => 'fos_message_inbox',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_fos_message_inbox;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_message_inbox'));
                }

                return $ret;
            }
            not_fos_message_inbox:

            // fos_message_sent
            if ('/message/sent' === $pathinfo) {
                return array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::sentAction',  '_route' => 'fos_message_sent',);
            }

            // fos_message_search
            if ('/message/search' === $pathinfo) {
                return array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::searchAction',  '_route' => 'fos_message_search',);
            }

            // fos_message_deleted
            if ('/message/deleted' === $pathinfo) {
                return array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::deletedAction',  '_route' => 'fos_message_deleted',);
            }

            // fos_message_thread_new
            if ('/message/new' === $pathinfo) {
                return array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::newThreadAction',  '_route' => 'fos_message_thread_new',);
            }

            // fos_message_thread_delete
            if (preg_match('#^/message/(?P<threadId>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_message_thread_delete']), array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::deleteAction',));
                if (!in_array($requestMethod, ['POST', 'DELETE'])) {
                    $allow = array_merge($allow, ['POST', 'DELETE']);
                    goto not_fos_message_thread_delete;
                }

                return $ret;
            }
            not_fos_message_thread_delete:

            // fos_message_thread_undelete
            if (preg_match('#^/message/(?P<threadId>[^/]++)/undelete$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_message_thread_undelete']), array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::undeleteAction',));
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_message_thread_undelete;
                }

                return $ret;
            }
            not_fos_message_thread_undelete:

            // fos_message_thread_view
            if (preg_match('#^/message/(?P<threadId>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_message_thread_view']), array (  '_controller' => 'FOS\\MessageBundle\\Controller\\MessageController::threadAction',));
            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
